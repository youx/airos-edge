/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <Eigen/Geometry>
#include <condition_variable>
#include <memory>
#include <queue>

#include "base/device_connect/camera/camera_base.h"

namespace airos {
namespace middleware {
namespace device_service {

class CameraServiceImpl;

class CameraService {
 public:
  using CameraImageData = base::device::CameraImageData;

  CameraService();
  ~CameraService();
  bool Init(const base::device::CameraInitConfig &config);
  std::shared_ptr<CameraImageData> GetCameraData(
      unsigned int timeout_ms = 0);  // 0 means block

  // static bool LoadExtrinsics(const std::string &yaml_file, Eigen::Matrix4d
  // &extrinsic_params);
  static bool LoadExtrinsics(const std::string &yaml_file,
                             Eigen::Affine3d &camera2world_pose);
  static bool LoadIntrinsics(const std::string &yaml_file,
                             Eigen::Matrix3f &intrinsic_params);
  static bool LoadGroundPlaneCoffe(const std::string &yaml_file,
                                   Eigen::Vector4d &ground_plane_coffe);

 private:
  CameraService(const CameraService &) = delete;
  CameraService(CameraService &&) = delete;

 private:
  std::shared_ptr<CameraServiceImpl> impl_;
};

}  // namespace device_service
}  // namespace middleware
}  // namespace airos