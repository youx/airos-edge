/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <cassert>
#include <iostream>
#include <string>

#include "gtest/gtest.h"

#define private public
#include "middleware/protocol/v2x_codec/v2x_encode_component.h"

namespace os {
namespace v2x {
namespace protocol {

class V2xEncodeTest : public ::testing::Test {
 public:
  V2xEncodeTest() {}
  virtual ~V2xEncodeTest() {}
  void SetUp() override {
    airos::middleware::AirRuntimeInit("v2x_codec_test");
    const std::string file =
        "/airos/middleware/protocol/v2x_codec/ut/testdata/"
        "message_frame_spat.pb.txt";
    if (!apollo::cyber::common::GetProtoFromFile(file, &standrad_frame_)) {
      AERROR << "read file conf failed!";
      return;
    }
  }

 protected:
  AIROS_COMPONENT_CLASS_NAME(V2xEncodeComponent) v2x_encode_component_;
  v2xpb::asn::MessageFrame standrad_frame_;
};

TEST_F(V2xEncodeTest, test_encode) {
  std::shared_ptr<v2xpb::asn::MessageFrame> frame = nullptr;
  std::shared_ptr<os::v2x::device::RSUData> encode_pb = nullptr;
  EXPECT_FALSE(v2x_encode_component_.MessageFrame2RsuPb(frame, encode_pb));
  frame = std::make_shared<v2xpb::asn::MessageFrame>();
  encode_pb = std::make_shared<os::v2x::device::RSUData>();
  EXPECT_FALSE(v2x_encode_component_.MessageFrame2RsuPb(frame, encode_pb));

  frame = std::make_shared<v2xpb::asn::MessageFrame>(standrad_frame_);
  EXPECT_TRUE(v2x_encode_component_.MessageFrame2RsuPb(frame, encode_pb));
  AINFO << encode_pb->DebugString();
}

}  // namespace protocol
}  // namespace v2x
}  // namespace os
