"""Loads the glfw library"""

# Sanitize a dependency so that it works correctly from code that includes
# Apollo as a submodule.
def clean_dep(dep):
    return str(Label(dep))

def repo():
    # gflags
    native.new_local_repository(
        name = "glfw",
        build_file = clean_dep("//third_party/glfw:glfw.BUILD"),
        path = "/opt/glfw",
    )