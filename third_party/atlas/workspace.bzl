def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "atlas",
        build_file = clean_dep("//third_party/atlas:atlas.BUILD"),
        path = "/opt/atlas"
    )