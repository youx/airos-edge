def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "yaml_cpp",
        build_file = clean_dep("//third_party/yaml_cpp:yaml_cpp.BUILD"),
        path = "/opt/yaml-cpp"
    )