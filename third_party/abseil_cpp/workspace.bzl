"""Load the abseil_cpp library"""

# Sanitize a dependency so that it works correctly from code that includes
# Apollo as a submodule.
def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "abseil_cpp",
        build_file = clean_dep("//third_party/abseil_cpp:abseil_cpp.BUILD"),
        path = "/opt/abseil_cpp",
    )