#! /usr/bin/env bash

TOP_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P)"
OUT_DIR="${TOP_DIR}/output"

bin_dir="${OUT_DIR}/bin"
export PATH=${bin_dir}:$PATH

# 3rd
for lib in $(ls "/opt/")
do
    if [ -e "/opt/${lib}" ]; then
        export LD_LIBRARY_PATH=/opt/${lib}/lib:$LD_LIBRARY_PATH
    fi
done
export LD_LIBRARY_PATH=${OUT_DIR}/3rd:$LD_LIBRARY_PATH

# protobuf
export LD_LIBRARY_PATH=${OUT_DIR}/protobuf:$LD_LIBRARY_PATH

# airosrt
export LD_LIBRARY_PATH=/opt/airosrt/so:$LD_LIBRARY_PATH

# airservice
export LD_LIBRARY_PATH=${OUT_DIR}/air_service/modules/perception-camera/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${OUT_DIR}/air_service/modules/perception-visualization/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${OUT_DIR}/air_service/modules/perception-fusion/lib:$LD_LIBRARY_PATH

# cyberRT
export CYBER_PATH="${OUT_DIR}/airosrt/cyber"
export PATH=$PATH:"${OUT_DIR}/airosrt/bin"
export PYTHONPATH="${OUT_DIR}/airosrt"

export CYBER_DOMAIN_ID=80
export CYBER_IP=127.0.0.1

export GLOG_log_dir="/home/caros/log"
mkdir -p ${GLOG_log_dir}

export GLOG_alsologtostderr=0
export GLOG_colorlogtostderr=1
export GLOG_minloglevel=1

export sysmo_start=0

# for DEBUG log
# export GLOG_v=4
