#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_atlas {
    local url="https://zhilu.bj.bcebos.com/atlas.tar.gz"
    local sha256="3ea00c613db92a1cd7723d85685ecbc03b955f38d4805ff95fe050e087e19604"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "atlas.tar.gz" "${sha256}"; then
            tar -zxf atlas.tar.gz
        else
            echo "check_sha256 failed: atlas.tar.gz"
        fi
        rm atlas.tar.gz
    popd >/dev/null
}

function main {
    install_atlas
}

main "$@"