#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_ffmpeg {
    local url="https://zhilu.bj.bcebos.com/ffmpeg.tar.gz"
    local sha256="73277d68ecfe0a1f880a1dacf8b781c76da8126191c9c09b46e45e49220bfb9e"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "ffmpeg.tar.gz" "${sha256}"; then
            tar -zxf ffmpeg.tar.gz
        else
            echo "check_sha256 failed: ffmpeg.tar.gz"
        fi
        rm ffmpeg.tar.gz
    popd >/dev/null
}

function main {
    install_ffmpeg
}

main "$@"