#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_opencv {
    local url="https://zhilu.bj.bcebos.com/opencv.tar.gz"
    local sha256="df3ac35b578ccc6a7ebebc820ca2a62977e20a82fcd7ed7adfe44cb2b67e0298"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "opencv.tar.gz" "${sha256}"; then
            tar -zxf opencv.tar.gz
        else
            echo "check_sha256 failed: opencv.tar.gz"
        fi
        rm opencv.tar.gz
    popd >/dev/null
}

function main {
    install_opencv
}

main "$@"