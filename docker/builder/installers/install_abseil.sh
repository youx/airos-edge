#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_abseil {
    local url="https://zhilu.bj.bcebos.com/abseil_cpp.tar.gz"
    local sha256="fff13baa0457fff5b1ddcce1fbf5e2add4cb3bc681ef5dc9a2bd69deda24e0dc"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "abseil_cpp.tar.gz" "${sha256}"; then
            tar -zxf abseil_cpp.tar.gz
            python3 -m pip install --timeout 30 --no-cache-dir protobuf 
        else
            echo "check_sha256 failed: abseil_cpp.tar.gz"
        fi
        rm abseil_cpp.tar.gz
    popd >/dev/null
}

function main {
    install_abseil
}

main "$@"